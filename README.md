#CATELIN: Catchment delineation WPS extension

A 52° north Web Processing Service extension based on TauDEM and GDAL.

## 1. Installation

This section will guide you to install the 52° North WPS and the Catelin extension. You can choose either a **runtime 
installation using Docker** or a **development installation** which will provide the source code of the components and the build process how-to.

> There are multiple ways to install and setup a WPS extension. For more information, please read the 52° North WPS [readme](https://github.com/52North/WPS/blob/dev/README.md).

### 1.1 Runtime installation using Docker

#### 1.1.1 Install docker

Depending on your operating system, choose the right one and follow the [instructions](https://docs.docker.com/engine/installation/) on Docker web site.

#### 1.1.2 Pull wps-catelin docker image

1 - Launch a docker terminal (MacOSX, Windows) or run terminal (Linux)

2 - Pull the image with the following command:
```
$ docker pull zuncle/wps-catelin
```

3 - Create a named container using the following command (it will create and run the container):
```
$ docker run -d -it --name catelin -p 8888:8000 zuncle/wps-catelin  
```

4 - To stop the container, use the following command:
```
$ docker stop catelin  
```

5 - To restart the container:
```
$ docker start catelin  
```

#### 1.1.3 Test Installation

In the following examples, the IP address uses by the container is assumed to be 192.168.99.100.

##### 1.1.3.1 GetCapabilities

Using a web browser, the following request shall returns the GetCapabilities of the 52° north WPS:

```
http://192.168.99.100:8888/wps/WebProcessingService?Request=GetCapabilities&Service=WPS
```
	
The process `fr.pixime.wps.catelin.CatchmentDelineationAlgorithm` shall appear in the response:

```
<wps:Process wps:processVersion="1.0.0">
    <ows:Identifier>fr.pixime.wps.catelin.CatchmentDelineationAlgorithm</ows:Identifier>
	<ows:Title>fr.pixime.wps.catelin.CatchmentDelineationAlgorithm</ows:Title>
</wps:Process>
```

##### 1.1.3.2 DescribeProcess

Using a web browser, the following request shall return the description of the catchment delineation algorithm:
```
http://192.168.99.100:8888/wps/WebProcessingService?Request=DescribeProcess&Service=WPS&version=1.0.0&identifier=fr.pixime.wps.catelin.CatchmentDelineationAlgorithm
```

##### 1.1.3.3 Catchment delineation

Using a web browser, the following url triggers a catchment delineation for the Logan DEM using an outlet located with NAD82/UTM zone 12N and coordinates (447560.16, 46112835.16):
```
http://192.168.99.100:8888/wps/WebProcessingService?Request=Execute&Service=WPS&version=1.0.0&identifier=fr.pixime.wps.catelin.CatchmentDelineationAlgorithm&DataInputs=Domain=Logan;X=447560.16;Y=4612835.91;SRS=EPSG:26912
```

You should get the following response indicating the geometry of the delineated catchment:
```
 <wps:ExecuteResponse xmlns:wps="http://www.opengis.net/wps/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ows="http://www.opengis.net/ows/1.1" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsExecute_response.xsd" serviceInstance="http://localhost:8080/wps/WebProcessingService?REQUEST=GetCapabilities&SERVICE=WPS" xml:lang="en-US" service="WPS" version="1.0.0">
    <wps:Process wps:processVersion="1.0.0">
        <ows:Identifier>fr.pixime.wps.catelin.CatchmentDelineationAlgorithm</ows:Identifier>
        <ows:Title>fr.pixime.wps.catelin.CatchmentDelineationAlgorithm</ows:Title>
    </wps:Process>
    <wps:Status creationTime="2016-02-09T10:25:55.699Z">
        <wps:ProcessSucceeded>Process successful</wps:ProcessSucceeded>
    </wps:Status>
    <wps:ProcessOutputs>
        <wps:Output>
        <ows:Identifier>FEATURES</ows:Identifier>
        <ows:Title>FEATURES</ows:Title>
        <wps:Data>
            <wps:ComplexData mimeType="application/json">
                {"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[451004.0191,4612862.4469],...
```

### 1.2 Development installation

#### 1.2.1 Get 52° North WPS source code

Clone 52° north WPS dev branch [source code](https://github.com/52North/WPS).
```
$ git clone https://github.com/52North/WPS.git
```
#### 1.2.2 Get Catelin WPS source code

In the WPS root directory, type the following git command:
```
$ git submodule add https://zuncle@bitbucket.org/zuncle/wps-catelin.git pixime-wps-catelin
```
This will create à directory for the sub module and clone the wps-catelin repository.

#### 1.2.3 Update WPS configuration file

Open wps_config.xml. Find the element <LocalAlgorithmRepository> and add the following properties in order to:

- activate the wps algorithm **fr.pixime.wps.catelin.CatchmentDelineationAlgorithm**,
- set the catelin data root directory where the DEM are expected to be found,
- set number of processes used by TauDEM mpiexec commands,
- eventually, specify a folder where the intermediate outputs will be copied during the process.

```
<Repository name="LocalAlgorithmRepository" className="org.n52.wps.server.LocalAlgorithmRepository" active="true">
   <Property name="Algorithm" active="true">fr.pixime.wps.catelin.CatchmentDelineationAlgorithm</Property>
   <Property name="Catelin.directory" active="true">/Users/pixime/dev/taudem/</Property>
   <Property name="Catelin.numberOfProcesses" active="true">8</Property>
   <Property name="Catelin.debugFolder" active="true">/Users/pixime/dev/taudem/debug</Property>
```

If these properties does not appear in the wps-config.xml (or their **active** attribute is set to false):

- Catelin.directory will be set to directory **~/catelin**.
- The number of processes is set to 8 by default.
- Intermediate files will not be copied.

#### 1.2.4 Compile 52° North WPS with wps-catelin

Run maven with the following options:
```
$ mvn clean install license:format -Dwps.config.file=WPS/52n-wps-webapp/src/main/webapp/config/wps_config.xml -DskipTests -Pwith-geotools
```

#### 1.2.5 Deploy a first DEM domain: uk90m

1. Create a directory on your computer, for example `/tauDEM/data`. Notice that the directory path shall match with the 
property `CatelinDirectory` defined above. 

2. Create the subdirectory uk90m in /tauDEM/data.

3. Download the files [demp.tif](https://bitbucket.org/zuncle/wps-catelin/src/93d788fa6c16/uk90m/) and [demsrc.tif](https://bitbucket.org/zuncle/wps-catelin/src/93d788fa6c16/uk90m/) and copy them in `/tauDEM/data/uk90m`.

#### 1.2.6 Test Installation

In the following examples, the use of the port 8080 is assumed by the Tomcat server, as well as the availability of the uk90m domain DEM.

##### 1.2.6.1 GetCapabilities

Using a web browser, the following request shall returns the GetCapabilities of the 52° north WPS:

```
http://localhost:8080/wps/WebProcessingService?Request=GetCapabilities&Service=WPS
```

The process `fr.pixime.wps.catelin.CatchmentDelineationAlgorithm` shall appear in the response:

```
<wps:Process wps:processVersion="1.0.0">
    <ows:Identifier>fr.pixime.wps.catelin.CatchmentDelineationAlgorithm</ows:Identifier>
	<ows:Title>fr.pixime.wps.catelin.CatchmentDelineationAlgorithm</ows:Title>
</wps:Process>
```

##### 1.2.6.2 DescribeProcess

Using a web browser, the following request shall return the description of the catchment delineation algorithm:

```
http://localhost:8080/wps/WebProcessingService?Request=DescribeProcess&Service=WPS&version=1.0.0&identifier=fr.pixime.wps.catelin.CatchmentDelineationAlgorithm
```

##### 1.2.6.3 Catchment delineation

Using a web browser, the following url triggers a catchment delineation for the uk90m DEM and an outlet with the latlon coordinates (53.6768, -0.6877):

```
http://localhost:8080/wps/WebProcessingService?Request=Execute&Service=WPS&version=1.0.0&identifier=fr.pixime.wps.catelin.CatchmentDelineationAlgorithm&DataInputs=Domain=uk90m;X=-0.6877;Y=53.6768;SRS=EPSG:4326
```

You should get the following response indicating the geometry of the delineated catchment:

```
<wps:ProcessOutputs>
    <wps:Output>
        <ows:Identifier>FEATURES</ows:Identifier>
        <ows:Title>FEATURES</ows:Title>
        <wps:Data>
            <wps:ComplexData mimeType="application/json">
                {"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"MultiPolygon","coordinates":[[[[-2.3233,52.9388],[-2.3233,52.9379], ...
                ...
```