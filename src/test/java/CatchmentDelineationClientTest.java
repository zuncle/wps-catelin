/**
 * ﻿Copyright (C) 2015 - 2014 Pixime Consulting
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 *
 * If the program is linked with libraries which are licensed under one of
 * the following licenses, the combination of the program with the linked
 * library is not considered a "derivative work" of the program:
 *
 *       • Apache License, version 2.0
 *       • Apache Software License, version 1.0
 *       • GNU Lesser General Public License, version 3
 *       • Mozilla Public License, versions 1.0, 1.1 and 2.0
 *       • Common Development and Distribution License (CDDL), version 1.0
 *
 * Therefore the distribution of the program linked with libraries licensed
 * under the aforementioned licenses, is permitted by the copyright holders
 * if the distribution is compliant with both the GNU General Public
 * License version 2 and the aforementioned licenses.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

import net.opengis.wps.x100.*;
import org.apache.xmlbeans.XmlException;
import org.geotools.feature.FeatureCollection;
import org.geotools.geojson.feature.FeatureJSON;
import org.junit.Test;
import org.n52.wps.client.ExecuteResponseAnalyser;
import org.n52.wps.client.WPSClientException;
import org.n52.wps.client.WPSClientSession;
import org.n52.wps.commons.WPSConfig;
import org.n52.wps.io.data.IData;
import org.n52.wps.io.data.binding.complex.GTVectorDataBinding;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import static org.junit.Assert.assertTrue;

/**
 * Created by xavierthomas on 12/12/15.
 */

public class CatchmentDelineationClientTest {
    String wpsURL = "http://localhost:8080/wps/WebProcessingService";
    String processID = "fr.pixime.wps.catelin.CatchmentDelineationAlgorithm";

    @Test
    public void getCapabilities() {
        try {
            CapabilitiesDocument capabilitiesDocument = requestGetCapabilities(wpsURL);
            System.out.println(capabilitiesDocument);
        } catch (WPSClientException e) {
            e.printStackTrace();
            assertTrue(false);
        }
        assertTrue(true);
    }

    @Test
    public void describeProcess() {
        try {
            ProcessDescriptionType describeProcessDocument = requestDescribeProcess(wpsURL, processID);
            System.out.println(describeProcessDocument);
        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        }
        assertTrue(true);
    }

    @Test
    public void executeProcess1(){
        executeProcess(447560.16, 4612835.91, "EPSG:26912", "logan");
    }

    @Test
    public void executeProcess2(){
        executeProcess(-1.62627, 52.09670, "EPSG:4326", "uk90m");
    }

    private void executeProcess(double x, double y, String srs, String domain){

        try {
            URL url = getClass().getResource("wps_config_custom.xml");
            WPSConfig.forceInitialization(url.getPath());
        } catch (XmlException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ProcessDescriptionType describeProcessDocument = requestDescribeProcess(wpsURL, processID);

            HashMap<String, Object> inputs = new HashMap<String, Object>();

            // Define inputs
            inputs.put("X", x);
            inputs.put("Y", y);
            inputs.put("SRS", srs);
            inputs.put("Domain", domain);

            IData data = requestExecuteProcess(wpsURL, processID, describeProcessDocument, inputs);

            // get the outputs
            if (data instanceof GTVectorDataBinding) {
                FeatureCollection featureCollection = ((GTVectorDataBinding) data)
                        .getPayload();

                // create GeoJSON file
                File tempFile = new File("result.json");
                new FeatureJSON().writeFeatureCollection(featureCollection, tempFile);
                System.out.println(String.format("Output available in file %s", tempFile.getAbsolutePath()));
            }

        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
        assertTrue(true);
    }

    private CapabilitiesDocument requestGetCapabilities(String url)
            throws WPSClientException {

        WPSClientSession wpsClient = WPSClientSession.getInstance();

        wpsClient.connect(url);

        CapabilitiesDocument capabilities = wpsClient.getWPSCaps(url);

        ProcessBriefType[] processList = capabilities.getCapabilities()
                .getProcessOfferings().getProcessArray();

        for (ProcessBriefType process : processList) {
            System.out.println(process.getIdentifier().getStringValue());
        }
        return capabilities;
    }

    private ProcessDescriptionType requestDescribeProcess(String url, String processID) throws IOException {

        WPSClientSession wpsClient = WPSClientSession.getInstance();

        ProcessDescriptionType processDescription = wpsClient.getProcessDescription(url, processID);

        InputDescriptionType[] inputList = processDescription.getDataInputs().getInputArray();

        for (InputDescriptionType input : inputList) {
            System.out.println(input.getIdentifier().getStringValue());
        }
        return processDescription;
    }

    public IData requestExecuteProcess(String url, String processID,
                                       ProcessDescriptionType processDescription,
                                       HashMap<String, Object> inputs) throws Exception {
        org.n52.wps.client.ExecuteRequestBuilder executeBuilder = new org.n52.wps.client.ExecuteRequestBuilder(
                processDescription);

        for (InputDescriptionType input : processDescription.getDataInputs()
                .getInputArray()) {
            String inputName = input.getIdentifier().getStringValue();
            Object inputValue = inputs.get(inputName);
            if (input.getLiteralData() != null) {
                if (inputValue instanceof String) {
                    executeBuilder.addLiteralData(inputName,
                            (String) inputValue);
                }
                if (inputValue instanceof Double) {
                    executeBuilder.addLiteralData(inputName,
                            inputValue.toString());
                }
            } else if (input.getComplexData() != null) {
                // Complexdata by value
                if (inputValue instanceof FeatureCollection) {
                    IData data = new GTVectorDataBinding(
                            (FeatureCollection) inputValue);
                    executeBuilder
                            .addComplexData(
                                    inputName,
                                    data,
                                    "http://schemas.opengis.net/gml/3.1.1/base/feature.xsd",
                                    null, "text/xml");
                }
                // Complexdata Reference
                if (inputValue instanceof String) {
                    executeBuilder
                            .addComplexDataReference(
                                    inputName,
                                    (String) inputValue,
                                    "http://schemas.opengis.net/gml/3.1.1/base/feature.xsd",
                                    null, "text/xml");
                }

                if (inputValue == null && input.getMinOccurs().intValue() > 0) {
                    throw new IOException("Property not set, but mandatory: "
                            + inputName);
                }
            }
        }
        // Result as JSON
        executeBuilder.setMimeTypeForOutput("application/json", "FEATURES");

        // Result as GML3
        //executeBuilder.setMimeTypeForOutput("text/xml", "result");
        //executeBuilder.setSchemaForOutput(
        //        "http://schemas.opengis.net/gml/3.1.1/base/feature.xsd",
        //        "result");

        ExecuteDocument execute = executeBuilder.getExecute();
        execute.getExecute().setService("WPS");
        WPSClientSession wpsClient = WPSClientSession.getInstance();
        Object responseObject = wpsClient.execute(url, execute);
        if (responseObject instanceof ExecuteResponseDocument) {
            ExecuteResponseDocument response = (ExecuteResponseDocument) responseObject;
            System.out.println(response);
            ExecuteResponseAnalyser analyser = new ExecuteResponseAnalyser(
                    execute, response, processDescription);
            IData data = (IData) analyser.getComplexDataByIndex(0,
                    GTVectorDataBinding.class);
            return data;
        }
        throw new Exception("Exception: " + responseObject.toString());
    }
}
