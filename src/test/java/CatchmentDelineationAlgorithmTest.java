/**
 * ﻿Copyright (C) 2015 - 2014 Pixime Consulting
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 *
 * If the program is linked with libraries which are licensed under one of
 * the following licenses, the combination of the program with the linked
 * library is not considered a "derivative work" of the program:
 *
 *       • Apache License, version 2.0
 *       • Apache Software License, version 1.0
 *       • GNU Lesser General Public License, version 3
 *       • Mozilla Public License, versions 1.0, 1.1 and 2.0
 *       • Common Development and Distribution License (CDDL), version 1.0
 *
 * Therefore the distribution of the program linked with libraries licensed
 * under the aforementioned licenses, is permitted by the copyright holders
 * if the distribution is compliant with both the GNU General Public
 * License version 2 and the aforementioned licenses.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */
import fr.pixime.wps.catelin.CatchmentDelineationAlgorithm;
import org.junit.Assert;
import org.junit.Test;
import org.n52.wps.io.data.IData;
import org.n52.wps.io.data.binding.complex.GTVectorDataBinding;
import org.n52.wps.io.data.binding.literal.LiteralDoubleBinding;
import org.n52.wps.io.data.binding.literal.LiteralStringBinding;
import org.n52.wps.io.datahandler.generator.GeoJSONGenerator;
import org.n52.wps.server.ExceptionReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pixime on 17/12/15.
 */
public class CatchmentDelineationAlgorithmTest {

    private static final Logger logger = LoggerFactory.getLogger(CatchmentDelineationAlgorithmTest.class);

    @Test
    public void test1() throws ExceptionReport, IOException{
        logger.info("Logan with EPSG:4326");
        runCatchmentDelineation(41.88793, -111.707525, "EPSG:4326", "logan");
    }

    @Test
    public void test2() throws ExceptionReport, IOException{
        logger.info("Logan with EPSG:26912");
        runCatchmentDelineation(447560.16, 4612835.91, "EPSG:26912", "logan");
    }

    @Test
    public void test3() throws ExceptionReport, IOException{
        logger.info("uk90m with EPSG:4326");
        runCatchmentDelineation(-1.62627, 52.09670, "EPSG:4326", "uk90m");
    }

    private void runCatchmentDelineation(double x, double y, String srs, String domain) throws ExceptionReport, IOException {

        LiteralDoubleBinding doubleBinding1 = new LiteralDoubleBinding(x);
        LiteralDoubleBinding doubleBinding2 = new LiteralDoubleBinding(y);
        LiteralStringBinding doubleBinding3 = new LiteralStringBinding(srs);
        LiteralStringBinding doubleBinding5 = new LiteralStringBinding(domain);

        List<IData> inputDataList1 = new ArrayList<IData>();
        inputDataList1.add(doubleBinding1);
        List<IData> inputDataList2 = new ArrayList<IData>();
        inputDataList2.add(doubleBinding2);
        List<IData> inputDataList3 = new ArrayList<IData>();
        inputDataList3.add(doubleBinding3);
        List<IData> inputDataList5 = new ArrayList<IData>();
        inputDataList5.add(doubleBinding5);

        Map<String,List<IData>> map = new HashMap<String, List<IData>>();
        map.put(CatchmentDelineationAlgorithm.INPUT1, inputDataList1);
        map.put(CatchmentDelineationAlgorithm.INPUT2, inputDataList2);
        map.put(CatchmentDelineationAlgorithm.INPUT3, inputDataList3);
        map.put(CatchmentDelineationAlgorithm.INPUT5, inputDataList5);

        CatchmentDelineationAlgorithm algo = new CatchmentDelineationAlgorithm();
        Map<String, IData> result = algo.run(map);

        Assert.assertTrue("Result not available!", result.get(CatchmentDelineationAlgorithm.OUTPUT) != null);
        logger.info("Succesfully retrieved result: '{}'", result.get(CatchmentDelineationAlgorithm.OUTPUT));
        IData data = result.get("FEATURES");
        Assert.assertTrue("Unexpected result", (data != null && data instanceof GTVectorDataBinding));

        // Print result to console
        GeoJSONGenerator gen = new GeoJSONGenerator();
        InputStream in = gen.generateStream(data, "", "");
        int size = 0;
        byte[] buffer = new byte[1024];
        while ((size = in.read(buffer)) != -1) System.out.write(buffer, 0, size);
    }
}