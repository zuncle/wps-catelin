/**
 * ﻿Copyright (C) 2015 - 2014 Pixime Consulting
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 *
 * If the program is linked with libraries which are licensed under one of
 * the following licenses, the combination of the program with the linked
 * library is not considered a "derivative work" of the program:
 *
 *       • Apache License, version 2.0
 *       • Apache Software License, version 1.0
 *       • GNU Lesser General Public License, version 3
 *       • Mozilla Public License, versions 1.0, 1.1 and 2.0
 *       • Common Development and Distribution License (CDDL), version 1.0
 *
 * Therefore the distribution of the program linked with libraries licensed
 * under the aforementioned licenses, is permitted by the copyright holders
 * if the distribution is compliant with both the GNU General Public
 * License version 2 and the aforementioned licenses.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */
package fr.pixime.wps.catelin;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.apache.commons.io.FileUtils;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.data.*;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.factory.Hints;
import org.geotools.feature.DefaultFeatureCollections;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.n52.wps.PropertyDocument;
import org.n52.wps.commons.WPSConfig;
import org.n52.wps.io.GTHelper;
import org.n52.wps.io.data.IData;
import org.n52.wps.io.data.binding.complex.GTVectorDataBinding;
import org.n52.wps.io.data.binding.literal.LiteralBooleanBinding;
import org.n52.wps.io.data.binding.literal.LiteralDoubleBinding;
import org.n52.wps.io.data.binding.literal.LiteralStringBinding;
import fr.pixime.wps.catelin.datahandler.parser.GML2BasicParser;
import org.n52.wps.server.AbstractSelfDescribingAlgorithm;
import org.n52.wps.server.ExceptionReport;
import org.n52.wps.server.LocalAlgorithmRepository;
import org.opengis.feature.Feature;
import org.opengis.feature.IllegalAttributeException;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.media.jai.JAI;
import java.io.*;
import java.net.MalformedURLException;
import java.util.*;

/**
 * Created by pixime on 17/12/15.
 */

// TODO
    // Outside bounding box
    // LOGGER (OK)
    // Exception
    // Nettoyage
    // Gestion debug file
    // Directory (OK)
    // Check components : TauDEM, Grass

public class CatchmentDelineationAlgorithm extends AbstractSelfDescribingAlgorithm {

    private static Logger LOGGER = LoggerFactory.getLogger(CatchmentDelineationAlgorithm.class);

    // if property Catelin.directory is not set, the domains folder will be found from {user.home}/DEFAULT_DATA_FOLDER
    private static String DEFAULT_DATA_FOLDER = "catelin";

    // Name of WPS inputs
    public static final String INPUT1 = "X";
    public static final String INPUT2 = "Y";
    public static final String INPUT3 = "SRS";
    public static final String INPUT4 = "MoveToStream";
    public static final String INPUT5 = "Domain";
    public static final String OUTPUT = "FEATURES";

    // properties of the process
    private String directory = "";
    private int numberOfProcesses = 8;
    private String debugFolder = "";

    // dem used to processes catchment delination
    private static String pName = "demp.tif";
    private static String srcName = "demsrc.tif";

    // filename of products or derivated products
    private final String saFilename         = "demssa.tif";
    private final String caFilename         = "demca.tif";
    private final String caVectorName       = "ca.gml";
    private final String caVectorShema      = "ca.xsd";
    private final String outletName         = "o.shp";
    private final String streamedOutletName = "so.shp";

    // usefull class members
    private Double x;       // outlet x-ccordinate input
    private Double y;       // outlet y-ccordinate input
    private Double s_x;     // streamed outlet x-ccordinate, if done
    private Double s_y;     // streamed outlet y-ccordinate, if done
    CoordinateReferenceSystem srcCrs;
    CoordinateReferenceSystem demCrs;
    private String pFilepath = "";      //example: /Users/xavierthomas/dev/taudem/exercices/workshop/demp.tif;
    private String srcFilepath = "";    // example: /Users/xavierthomas/dev/taudem/exercices/workshop/logansrc.tif
    private String domain = "";
    private SimpleFeatureType featureType;
    private List<File> filesToDelete = new ArrayList<File>();

    @Override
    public List<String> getInputIdentifiers() {
        List<String> identifierList =  new ArrayList<String>();
        identifierList.add(INPUT1);
        identifierList.add(INPUT2);
        identifierList.add(INPUT3);
        identifierList.add(INPUT4);
        identifierList.add(INPUT5);
        return identifierList;
    }

    @Override
    public List<String> getOutputIdentifiers() {
        List<String> identifierList =  new ArrayList<String>();
        identifierList.add(OUTPUT);
        return identifierList;
    }

    @Override
    public Class<?> getInputDataType(String id) {
        if(id.equalsIgnoreCase(INPUT1)){
            return LiteralDoubleBinding.class;
        }else if(id.equalsIgnoreCase(INPUT2)){
            return LiteralDoubleBinding.class;
        }else if(id.equalsIgnoreCase(INPUT3)){
            return LiteralStringBinding.class;
        }else if(id.equalsIgnoreCase(INPUT4)){
            return LiteralBooleanBinding.class;
        }else if(id.equalsIgnoreCase(INPUT5)){
            return LiteralStringBinding.class;
        }
        throw new RuntimeException("Error in GetInputDataType : Identifier " + id + " not found in process definition");
        //return null;
    }

    @Override
    public Class<?> getOutputDataType(String id) {
        if(id.equalsIgnoreCase(OUTPUT)){
            return GTVectorDataBinding.class;
        }
        throw new RuntimeException("Error in GetOutputDataType : Identifier " + id + " not found in process definition");
    }

    @Override
    public Map<String, IData> run(Map<String, List<IData>> inputData) throws ExceptionReport {
        try {
            return runAndClean(inputData);
        }catch (Exception e){
            throw e;
        }finally{
            deleteFiles();
        }
    }

    /**
     * Get the TauDEM geoTiff filenames based on DIRECTORY property (config.xml) and input parameter Domain
     */
    private void getTauDEMFilenames() throws ExceptionReport {

        // retrieve properties from configuration (wps-config.xml or equivalent)
        PropertyDocument.Property[] propertyArray =
                WPSConfig.getInstance().
                        getPropertiesForRepositoryClass(LocalAlgorithmRepository.class.getCanonicalName());

        for (PropertyDocument.Property property : propertyArray) {
            if (property.getName().equalsIgnoreCase(
                    WPSConfigVariables.DIRECTORY.toString())) {
                directory = property.getStringValue();
            }
            if (property.getName().equalsIgnoreCase(
                    WPSConfigVariables.NUMBER_OF_PROCESSES.toString()) && property.getActive() == true) {
                try {
                    numberOfProcesses = Integer.parseInt(property.getStringValue());
                }
                catch (NumberFormatException e){
                    LOGGER.warn(String.format("Found an unparsable number of processes in the configuration", pFilepath));
                }
            }
            if (property.getName().equalsIgnoreCase(
                    WPSConfigVariables.DEBUG_FOLDER.toString()) && property.getActive() == true) {
                debugFolder = property.getStringValue();
            }
        }

        LOGGER.info(String.format("The number of processes is %d", numberOfProcesses));

        if (directory.isEmpty()) {
            LOGGER.warn(String.format("Property %s not set in config", WPSConfigVariables.DIRECTORY.toString()));
            directory = String.format("%s%s%s%s",
                    System.getProperty("user.home"), File.separator,
                    DEFAULT_DATA_FOLDER, File.separator);
        }

        LOGGER.info("TauDEM files should be found from this directory: " + directory);

        //Check if directory exists
        File dir = new File(directory);
        if ( !dir.exists() || !dir.isDirectory() ) {
            throw new ExceptionReport("TauDEM data directory does not exist",
                    ExceptionReport.NO_APPLICABLE_CODE);
        }

        if (domain.isEmpty()){
            pFilepath = String.format("%s%s%s", directory, File.separator, pName);
            srcFilepath = String.format("%s%s%s", directory, File.separator, srcName);
        }
        else{
            pFilepath = String.format("%s%s%s%s%s",
                    directory, File.separator,
                    domain, File.separator,
                    pName);
            srcFilepath = String.format("%s%s%s%s%s",
                    directory, File.separator,
                    domain, File.separator,
                    srcName);
        }

        //Check if files exist and names are valid
        File file = new File(pFilepath);
        if ( !file.exists() || !file.isFile() ){
            LOGGER.error(String.format("TauDEM file %s not found", pFilepath));
            String msg = String.format("TauDEM file %s not found for domain %s", pName, domain.isEmpty()?"<empty>":domain);
            throw new ExceptionReport(msg, ExceptionReport.NO_APPLICABLE_CODE);
        }
        file = new File(srcFilepath);
        if ( !file.exists() || !file.isFile() ){
            LOGGER.error(String.format("TauDEM file %s not found", srcFilepath));
            String msg = String.format("TauDEM file %s not found for domain %s", srcName, domain.isEmpty()?"<empty>":domain);
            throw new ExceptionReport(msg, ExceptionReport.NO_APPLICABLE_CODE);
        }
    }

    /**
     * The process
     * @param inputData
     * @return
     */
    private Map<String, IData> runAndClean(Map<String, List<IData>> inputData) throws ExceptionReport {

        // get the WPS parameters values
        List<IData> xDataList = inputData.get(INPUT1);
        if(xDataList == null || xDataList.size() != 1){
            throw new ExceptionReport("Input parameter X is missing", ExceptionReport.MISSING_PARAMETER_VALUE);
        }

        try {
            this.x = ((LiteralDoubleBinding) xDataList.get(0)).getPayload();
            this.s_x = x;
        }
        catch (Exception e){
            throw new ExceptionReport("Expected type for input parameter X is Double", ExceptionReport.INVALID_PARAMETER_VALUE);
        }

        List<IData> yDataList = inputData.get(INPUT2);
        if(yDataList == null || yDataList.size() != 1){
            throw new ExceptionReport("Input parameter Y is missing", ExceptionReport.MISSING_PARAMETER_VALUE);
        }

        try {
            this.y = ((LiteralDoubleBinding) yDataList.get(0)).getPayload();
            this.s_y = y;
        }
        catch (Exception e){
            throw new ExceptionReport("Expected type for input parameter Y is Double", ExceptionReport.INVALID_PARAMETER_VALUE);
        }

        List<IData> srsDataList = inputData.get(INPUT3);
        if(srsDataList == null || srsDataList.size() != 1){
            throw new ExceptionReport("Input parameter SRS is missing", ExceptionReport.MISSING_PARAMETER_VALUE);
        }

        String srs;
        try {
            srs = ((LiteralStringBinding) srsDataList.get(0)).getPayload();
        }
        catch (Exception e){
            throw new ExceptionReport("Expected type for input parameter SRS is String", ExceptionReport.INVALID_PARAMETER_VALUE);
        }

        try {
            this.srcCrs = CRS.decode(srs);
        }
        catch(Exception e){
            throw new ExceptionReport(
                    String.format("Unknown Spatial Reference System: %s", srs),
                    ExceptionReport.INVALID_PARAMETER_VALUE);
        }

        Boolean moveToStream = true;
        List<IData> moveToStreamDataList = inputData.get(INPUT4);
        if(moveToStreamDataList != null && moveToStreamDataList.size() == 1){
            moveToStream = ((LiteralBooleanBinding) srsDataList.get(0)).getPayload();
        }

        List<IData> domainList = inputData.get(INPUT5);
        if(domainList != null && domainList.size() == 1){
            this.domain = ((LiteralStringBinding) domainList.get(0)).getPayload();
        }

        getTauDEMFilenames();

        // Get the SRS of the demp file : demCrs
        File pFile = new File(pFilepath);
        try {
            this.demCrs = this.getTiffSRS(pFile);
        }
        catch(Exception e){
            throw new ExceptionReport(
                    String.format("Unable to detect Coordinate Reference System in TauDEM file %s", pName),
                    ExceptionReport.NO_APPLICABLE_CODE);
        }

        // Create outlet shapefile from input coordinates
        File tempDirectory = createTempDirectory();
        filesToDelete.add(tempDirectory);
        FeatureCollection outletFeatures = createOutletFeatures();
        File outletSHPFile;
        try {
            outletSHPFile = createShapefile(outletFeatures, tempDirectory);
        } catch (IOException e) {
            throw new ExceptionReport("Unable to create shapefile", ExceptionReport.NO_APPLICABLE_CODE);
        }

        LOGGER.info("Output files will be stored in temporary directory: " + tempDirectory.getAbsolutePath());

        // Run TauDEM mpiexec commands
        runTauDEM(tempDirectory, outletSHPFile, moveToStream);

        // Create features output
        File out = new File(String.format("%s%s%s", tempDirectory.getAbsolutePath(), File.separator, this.caVectorName));

        GML2BasicParser parser = new GML2BasicParser();

        // because GML file produced by GDAL refers to the sheae file ca.xsd, we need to relocate it in the current folder:
        try {
            File caVectorShemaFile = new File(this.caVectorShema);
            if ( caVectorShemaFile.exists())
                FileUtils.forceDelete(caVectorShemaFile);
            FileUtils.moveFile(
                    new File(String.format("%s%s%s", tempDirectory.getAbsolutePath(), File.separator, this.caVectorShema)),
                    caVectorShemaFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        GTVectorDataBinding bind = parser.parseXML(out);
        FeatureCollection features = bind.getPayload();
        FeatureCollection fOut = DefaultFeatureCollections.newCollection();

        try {
            MathTransform tx = CRS.findMathTransform(this.demCrs, this.srcCrs, true);

            final SimpleFeatureType type = DataUtilities.createType("ca",
                    "the_geom:Polygon," +
                    "domain:String," +
                    "x:Float," +
                    "y:Float," +
                    "s_x:Float," +
                    "s_y:Float"

            );
            SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(type);

            FeatureIterator<?> featureIterator = features.features();

            while (featureIterator.hasNext()) {
                SimpleFeature feature = (SimpleFeature) featureIterator.next();

                // Convert output to SRS Coordinate Reference System
                Geometry geometry = (Geometry) feature.getDefaultGeometry();
                Coordinate[] coords = geometry.getCoordinates();
                for (Coordinate coordinate : coords) {
                    Coordinate k = new Coordinate();
                    k = JTS.transform(coordinate, k, tx);
                }
                Geometry newGeometry = JTS.transform(geometry, tx);

                featureBuilder.add(newGeometry);
                featureBuilder.add(domain);
                featureBuilder.add((new Float(x)));
                featureBuilder.add((new Float(y)));
                featureBuilder.add((new Float(s_x)));
                featureBuilder.add((new Float(s_y)));
                SimpleFeature newFeature = featureBuilder.buildFeature(null);

                fOut.add(newFeature);
            }
        } catch (FactoryException e) {
            throw new ExceptionReport("Unable to find coordinates transformation", ExceptionReport.NO_APPLICABLE_CODE);
        } catch (TransformException e) {
            throw new ExceptionReport("Unable to convert coordinates of output features", ExceptionReport.NO_APPLICABLE_CODE);
        } catch (SchemaException e) {
            throw new ExceptionReport("Unable to ccreate feature type", ExceptionReport.NO_APPLICABLE_CODE);
        }

        // Prepare result
        HashMap<String, IData> result = new HashMap<String, IData>();
        result.put(OUTPUT, new GTVectorDataBinding(fOut));
        return result;
    }

    private CoordinateReferenceSystem getTiffSRS(File file){
        JAI.getDefaultInstance().getTileCache().setMemoryCapacity(256*1024*1024);

        Hints hints = new Hints(Hints.FORCE_LONGITUDE_FIRST_AXIS_ORDER,
                Boolean.TRUE);
        GeoTiffReader reader;
        try {
            reader = new GeoTiffReader(file, hints);
            GridCoverage2D coverage = reader.read(null);
            return coverage.getGridGeometry().getCoordinateReferenceSystem2D();
        } catch (DataSourceException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Create features from outlet coordinates using a coordinates transformation
     * @return
     */
    private FeatureCollection createOutletFeatures(){
        File result;

        String uuid = "0";
        MathTransform transform = null;
        try {
            transform = CRS.findMathTransform(srcCrs, demCrs, true);
        } catch (FactoryException e) {
            throw new RuntimeException("Reference system error: unable to create CRS transformation");
        }

        // transformation coordinates
        double[] p = new double[]{x, y};
        if ( !this.srcCrs.getName().getCode().equals(this.demCrs.getName().getCode()) ){
            try {
                transform.transform(p, 0, p, 0, 1);
            } catch (TransformException e) {
                throw new RuntimeException("Reference system error: unable to apply CRS transformation");
            }
        }

        GeometryFactory gf = new GeometryFactory();
        Coordinate coordinate = new Coordinate(p[0], p[1]);
        Point point = gf.createPoint( coordinate );

        SimpleFeatureType featureType = GTHelper.createFeatureType(point, uuid, demCrs);
        Feature feature = GTHelper.createFeature("0", point, featureType);
        SimpleFeatureCollection features = DefaultFeatureCollections.newCollection();

        features.add((SimpleFeature) feature);

        return features;
    }

    /**
     * Run shell command
     * @param cmd : The command
     * @param workingDirectory : The working directory
     * @return true if success, otherwise false
     * @throws IOException
     * @throws InterruptedException
     */
    private boolean runCmd(String cmd, File workingDirectory) throws IOException, InterruptedException {
        LOGGER.info("Run " + cmd);
        Process process = Runtime.getRuntime().exec(cmd, null, workingDirectory);
        process.waitFor();
        String line;
        InputStream is = (process.exitValue() == 0 ) ? process.getInputStream(): process.getErrorStream();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            if ( process.exitValue() == 0 )
                LOGGER.trace(line);
            else
                LOGGER.error(line);
        }
        br.close();
        return (process.exitValue() == 0);
    }

    /**
     * Run all TauDEM commands
     * @param workingDirectory
     * @param outletSHPFile
     * @param moveToStream
     * @throws ExceptionReport
     */
    private void runTauDEM(File workingDirectory, File outletSHPFile, boolean moveToStream) throws ExceptionReport {
        String cmd = "";
        try {
            if ( moveToStream ){
                // mpiexec -n 8 moveoutletstostrm -p demp.tif -src demsrc.tif -o o.shp -om so.shp
                cmd = String.format("mpiexec -n %d moveoutletstostrm -p %s -src %s -o %s -om %s",
                        this.numberOfProcesses,
                        this.pFilepath,
                        this.srcFilepath,
                        outletSHPFile.getName(),
                        streamedOutletName);
                runCmd(cmd, workingDirectory);
                // In this case, the outlet is replace by the streamed outlet:
                outletSHPFile = new File(String.format("%s/%s", workingDirectory, streamedOutletName));
                readStreamedOutletCoordinatesFromShapefile(workingDirectory);
            }

            // mpiexec -n 8 Aread8 -p demp.tif -o so.shp -ad8 demssa.tif
            cmd = String.format("mpiexec -n %d aread8 -p %s -o %s -ad8 %s -nc",
                    this.numberOfProcesses,
                    this.pFilepath,
                    outletSHPFile.getName(),
                    this.saFilename);
            runCmd(cmd, workingDirectory);

            // mpiexec -n 8 Threshold -ssa demssa.tif -src demca.tif -thresh 1
            cmd = String.format("mpiexec -n %d threshold -ssa %s -src %s -thresh 1",
                    this.numberOfProcesses,
                    this.saFilename,
                    this.caFilename);
            runCmd(cmd, workingDirectory);

            //gdal_polygonize.py demca.tif.
            cmd = String.format("gdal_polygonize.py %s%s%s -f GML %s%s%s ca ID",
                    workingDirectory, File.separator, this.caFilename,
                    workingDirectory, File.separator, this.caVectorName);
            runCmd(cmd, workingDirectory);

            // copy data to debug folder if folder property is set and valid
            File debugDirectory = new File(this.debugFolder);
            if ( debugDirectory.exists() ) {
                LOGGER.info(String.format("Copy output files from %s to %s", workingDirectory, debugFolder));
                for (File file : workingDirectory.listFiles())
                    FileUtils.copyFileToDirectory(file, debugDirectory);
            }

        } catch (Exception e) {
            throw new ExceptionReport(e.getMessage(), ExceptionReport.NO_APPLICABLE_CODE);
        }

    }

    /**
     * Create a temp directory in the File system to store intermediate files
     * @return The newly created temporary directory
     */
    private File createTempDirectory() throws ExceptionReport {
        File tempBaseFile = null;
        try {
            tempBaseFile = File.createTempFile("resolveDir", ".tmp");
            filesToDelete.add(tempBaseFile);
        } catch (IOException e) {
            throw new ExceptionReport("Unable to get temp directory.", ExceptionReport.NO_APPLICABLE_CODE);
        }

        File parent = tempBaseFile.getParentFile();
        if (parent == null || !parent.isDirectory())
            throw new ExceptionReport("Could not find temporary file directory.", ExceptionReport.NO_APPLICABLE_CODE);

        File tempDirectory = new File(parent, UUID.randomUUID().toString());

        if (!tempDirectory.mkdir()) {
            throw new ExceptionReport("Could not create temporary shp directory.", ExceptionReport.NO_APPLICABLE_CODE);
        }
        return tempDirectory;
    }


    /**
     * Create a shapefile in a temporary folder from a featureCollection
     * @param features The FeatureCollection
     * @return A File corresponding to the newly created file (.shp)
     * @throws IOException
     * @throws IllegalAttributeException
     */
    private File createShapefile(FeatureCollection features, File directory)
            throws IOException {

        File outletFile = new File(String.format("%s%s%s", directory.getAbsolutePath(), File.separator, this.outletName));
        DataStoreFactorySpi dataStoreFactory = new ShapefileDataStoreFactory();
        Map<String, Serializable> params = new HashMap<String, Serializable>();
        params.put("url", outletFile.toURI().toURL());
        params.put("create spatial index", Boolean.TRUE);

        ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
        newDataStore.createSchema((SimpleFeatureType) features.getSchema());
        Transaction transaction = new DefaultTransaction("create");

        String typeName = newDataStore.getTypeNames()[0];
        FeatureStore<SimpleFeatureType, SimpleFeature> featureStore =
                (FeatureStore<SimpleFeatureType, SimpleFeature>) newDataStore.getFeatureSource(typeName);
        featureStore.setTransaction(transaction);
        try {
            featureStore.addFeatures(features);
            transaction.commit();
            LOGGER.info(String.format("Outlet file %s created", this.outletName));
        } catch (Exception problem) {
            transaction.rollback();
        } finally {
            transaction.close();
        }
        return outletFile;
    }


    /**
     * Read the streamed outlet shapefile if present
     * @param directory
     * @return A featureCollection if the file is found, otherwise null
     * @throws IOException
     */
    private void readStreamedOutletCoordinatesFromShapefile(File directory) throws IOException {

        DataStore dataStore = null;
        FeatureIterator featureIterator = null;
        try {
            File streamedOutletFile = new File(String.format("%s%s%s", directory.getAbsolutePath(), File.separator, this.streamedOutletName));
            if (!streamedOutletFile.exists()) return;
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("url", streamedOutletFile.toURI().toURL());
            dataStore = DataStoreFinder.getDataStore(map);
            String typeName = dataStore.getTypeNames()[0];

            FeatureSource<SimpleFeatureType, SimpleFeature> source = dataStore
                    .getFeatureSource(typeName);
            FeatureCollection<SimpleFeatureType, SimpleFeature> features = source.getFeatures();

            if (features != null){
                featureIterator = features.features();
                if (featureIterator.hasNext()) {
                    featureIterator = features.features();
                    // get the first feature, shall be a point
                    SimpleFeature feature = (SimpleFeature) featureIterator.next();
                    Point point = (Point) feature.getDefaultGeometry();
                    // Change coordinate system from demSRS to srcCrs
                    // transformation coordinates
                    if (!this.srcCrs.getName().getCode().equals(this.demCrs.getName().getCode())) {

                        MathTransform transform = null;
                        try {
                            transform = CRS.findMathTransform(this.demCrs, srcCrs, true);
                        } catch (FactoryException e) {
                            throw new RuntimeException("Reference system error: unable to create CRS transformation");
                        }

                        double[] p = new double[]{point.getX(), point.getY()};
                        try {
                            transform.transform(p, 0, p, 0, 1);
                            s_x = p[0];
                            s_y = p[1];
                        } catch (TransformException e) {
                            throw new RuntimeException("Reference system error: unable to apply CRS transformation");
                        }
                    }
                }
            }
        }
        catch (IOException e){
            throw e;
        }
        finally{
            if (featureIterator != null) featureIterator.close();
            if (dataStore != null) dataStore.dispose();
        }
    }

    /**
     * Utility: Clean directory recursively
     * @param dir
     */
    private void removeDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null && files.length > 0) {
                for (File aFile : files) {
                    removeDirectory(aFile);
                }
            }
            dir.delete();
        } else {
            dir.delete();
        }
    }

    /**
     * Delete temporary folders
     */
    private void deleteFiles(){
        for(File file: this.filesToDelete)
            removeDirectory(file);
        LOGGER.info("Temporary files have been removed");
    }
}
